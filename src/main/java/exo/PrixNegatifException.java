package exo;

public class PrixNegatifException extends Exception{
	private double prixHT;
	public PrixNegatifException(double prixHT) {
		// TODO Auto-generated constructor stub
		super("Prix négatif " + prixHT );
		this.prixHT = prixHT;
	}
	public double getPrixHT() {
		return prixHT;
	}
	
	
}
