package lestests;

import org.junit.Assert;
import org.junit.Test;

public class MaPremiereClasseDeTest {
	
	@Test
	public void abs_QuandNombrePositif_AlorsLeResultatEstLeNombreTest() {
		//Arrange
			double leNombre = 1234;
			
		//Act
			double resultat = Math.abs(leNombre);
					
		//Assert
			Assert.assertEquals(leNombre, resultat, 0);
	}
	
	@Test
	public void abs_QuandNombreNegatif_AlorsLeResultatEstLaValeurAbsolueTest() {
		//Arrange
			double leNombre = -1234;
			
		//Act
			double resultat = Math.abs(leNombre);
					
		//Assert typique a jUnit et est obligatoire
			Assert.assertEquals(-leNombre, resultat, 0);
	}
	
	@Test
	public void abs_QuandNombreZero_AlorsLeResultatEstZero() {
		//Arrange
			double leNombre = 0;
			
		//Act
			double resultat = Math.abs(leNombre);
					
		//Assert typique a jUnit et est obligatoire
			Assert.assertEquals(0, resultat, 0);
	}
}
