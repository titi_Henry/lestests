package exo;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

public class ArticleTest {
	@Test
	public void calculTTC_QuandLePrixHTest1000_AlorsLePrixTTCEst1206Test() throws Exception {
		Article article = new Article("dummy reference", "dummy designation", 1000d);
		
		double prixTTC = article.calculTTC();
		
		Assert.assertEquals(1206, prixTTC, 0);
	}
	
	@Test
	public void setPrixHT_QuandLePrixEstNegatif_AlorsPrixNegatifException() throws Exception {
		Article article = new Article("dummy reference", "dummy designation");
		try {
					article.setPrixHT(-10);
					Assert.fail("PrixNegatifException attendu");
		} catch (PrixNegatifException e) {
			Assert.assertEquals(-10,e.getPrixHT(),0);
		}
		
		
	}
}
